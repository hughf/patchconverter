# Patch Converter
This is a simple shell script to convert patches written as zip files to Linux tarball
(i.e. .tar.gz) format.
For a little context, this project is aimed at OpenText Content Server/xECM professionals
who need to convert their Windows patch files to run on Linux, either directly in a Linux
VM, or in a K8S cluster.  Ultimately the converted patch files can be used in init images
to be supplied you xECM running in Kubernetes.

## Usage / Mode d'emploi
To use, 
* download this project. It consists of a single shell file. You can do git clone, or just download the shell script.
* In the location you intend to run it, create 3 directories - src, staging, and target
* place all the patch files you want zipped into the src directory
* run the script

The end result should be one tarball patch file for each patch zip file in the target folder.
For example, if you had two patches named pat12345.zip and pat23456.zip, you should end up with two tarballs named pat12345.tar.gz and pat23456.tar.gz
