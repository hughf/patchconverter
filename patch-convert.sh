#!/bin/bash
# This is a little script to convert patches for classic deployments, that is, zip files,
# to tar-balls for those clients using Linux and more importantly, kubernetes.
# Eventually, this will be part of a larger pipeline to build init images
# for patches. 
# For the init images (which get run as init containers in a K8S xECM deployment)
# there is some flexibility - offer init images for each patch, or bundle a group of patches
# together.
WORKDIR=`pwd`
SRCDIR=${WORKDIR}/src
STAGING=${WORKDIR}/staging
TARGETDIR=${WORKDIR}/target

for file in ${SRCDIR}/*; do
   if [ ! -f "$file" ] ; then 
      echo ${file} not a file, skipping...
   fi
   if [[ ! $file == *.zip ]]; then 
	echo ${file} is not a zip file, skipping...
   fi

   # Remove the extension to use for our tarball name
   # Below echoes the filename then cuts using -f 1 for field 1, and -d '.'
   # as delimeter
   FILENAME=$(basename $file)
   PREFIX=$(echo $FILENAME | cut -f 1 -d '.')
   echo "Processing patch file ${FILENAME}"
   echo "Unpacking to staging directory..."
   unzip $file -d ${STAGING}

   echo "patch file unpacked"
   echo "creating tarball ${PREFIX}.tar.gz in ${TARGETDIR}..."
   
   # A little explanation: in order to get a tarball with just
   # contents like 
   # patch/
   # patch/patnnnnn.txt
   # module/
   # module/ccmemlc_YY_0_Q/
   # ...
   # I need to CD into the staging where I unpacked the zip file
   # and execute with the czf option (c to compress, z to zip, and f to designate the file)
   # the -C is to lop off the leading ./ that would otherwise appear in my archive
   # and using * instead of . is to get the contents.
   cd ${STAGING}
   tar -czf ${TARGETDIR}/${PREFIX}.tar.gz -C . *
   echo "${PREFIX}.tar.gz created"
   # While still in staging, remove everything
   rm -R *
   cd ${WORKDIR}
  
done
